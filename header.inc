<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <link rel="icon" type="image/png" href="favicon.png">
    <link rel="stylesheet" href="style.css" type="text/css" media="screen" />
    <title>Qu'est-ce qu'on fait maintenant ?</title>
  </head>
  <body>
    <header>
      <nav>
        <ul>
          <li><a href="index.php" <?php if ($page == 'index') { echo 'class="active"';} ?> >Que faire ?</a></li>
          <li><a href="tasks.php" <?php if ($page == 'tasks') { echo 'class="active"';} ?> >Liste des tâches</a></li>
<!--          <li><a href="log/" <?php if ($page == 'log') { echo 'class="active"';} ?> >Tâches effectuées</a></li> -->
        </ul>
      </nav>
      <!-- <p class="info"></p> -->
      <!-- <p class="alert"></p> -->
    </header>
    <main>
