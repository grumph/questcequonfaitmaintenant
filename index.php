<?php

$POSTPONE_DELAY = '600'; // seconds

include 'init.inc';

if (isset($_POST['action']) and isset($_POST['task_id'])) {
    switch ($_POST['action']) {
    case 'later': {
        postpone_task($_POST['task_id'], $POSTPONE_DELAY);
        break;
    }
    case 'go': {
        postpone_task($_POST['task_id']);
        break;
    }
    default:
        # error !!!
        return;
    }
}


$first_task = get_task();

$page='index';
include 'header.inc';

if ($first_task == null) {
    echo "<p>Il faut ajouter des tâches</p>";
} else {
    if (strtotime($current_date) > strtotime($first_task['next'])) {
?>
      <form method="post">
        <input type="hidden" name="task_id" value="<?php echo $first_task['id']; ?>" />
        <input type="hidden" name="task_frequency" value="<?php echo $first_task['frequency']; ?>" />
        <p><?php echo $first_task['description']; ?></p>
        <button type="submit" name="action" value="later">Plus tard &#8631;</button>
        <button type="submit" name="action" value="go">&#10003; C'est fait !</button>
      </form>
<?php
    } else /* first_task['next'] is greater than now */ {
        echo "<p>C'est l'heure de glander &#9996;</p>";
    }
}


include 'footer.inc';

?>
