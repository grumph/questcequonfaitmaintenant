<?php

include 'init.inc';

$page='tasks';
include 'header.inc';

// Postpone a done task
if (isset($_POST['done_task'])) {
    postpone_task($_POST['done_task']);
}

// Delete a task
if (isset($_POST['delete_task'])) {
    delete_task($_POST['delete_task']);
}

// Update/Add a task
if (isset($_POST['update_task'])) {
    $new_frequency = mydatetime_in_seconds(
        $_POST['edit_frequency_months'],
        $_POST['edit_frequency_weeks'],
        $_POST['edit_frequency_days'],
        $_POST['edit_frequency_hours'],
        $_POST['edit_frequency_minutes'],
        $_POST['edit_frequency_seconds']);
    if ($_POST['edit_id'] == -1) {
        add_task($_POST['edit_description'], $new_frequency, $_POST['edit_next']);
    } else {
    update_task($_POST['edit_id'], $_POST['edit_description'], $new_frequency, $_POST['edit_next']);
    }
}

// Edit a task
$task_to_edit = null;
if (isset($_POST['edit_task'])) {
    $task_to_edit = get_task($_POST['edit_task']);
}

?>

<table>
   <thead>
      <tr>
         <th>ID</th>
         <th>Description</th>
         <th>Fréquence</th>
         <th>Prochaine occurence</th>
         <th class="tab_options">Options</th>
      </tr>
   </thead>
   <tbody>
<?php
    foreach (generate_all_tasks() as $task) {
?>
      <tr>
    <td><?php echo $task['id']; ?></td>
<td><?php echo $task['description']; ?></td>
<td><?php
    echo seconds_to_string($task['frequency']);
?></td>
<?php
    $timediff = strtotime($task['next']) - strtotime($current_date);
    echo '<td';
    if ($timediff < 1) {
        echo ' class="todo"';
    }
    echo ' aria-label="', $task['next'], '">';
    if ($timediff < 1) {
        echo 'À faire !';
    } else {
        echo 'dans ', seconds_to_string($timediff);
    }
    echo '</td>';

?>
    <td class="tab_options">
    <form method="post">
        <input type="checkbox" class="hidden" id="options<?php echo $task['id']; ?>">
        <div>
            <label for="options<?php echo $task['id']; ?>" aria-label="Options">&#9881;</label>
            <label for="options<?php echo $task['id']; ?>" aria-label="retour">&#8592;</label>
            <button type="submit" name="done_task" value="<?php echo $task['id']; ?>" aria-label="Fait !" >&#9989;</button>
            <button type="submit" name="edit_task" value="<?php echo $task['id']; ?>" aria-label="Modifier" >&#10000;</button>
            <button type="submit" name="delete_task" value="<?php echo $task['id']; ?>" aria-label="Supprimer" >&#10062;</button>
        </div>
    </form>
    </td>
</tr>
<?php
}
?>
   </tbody>
</table>

<?php

if ($task_to_edit !== null) {
    $id = $task_to_edit['id'];
    $description = $task_to_edit['description'];
    $frequency_struct = seconds_in_mydatetime($task_to_edit['frequency']);
    $next = $task_to_edit['next'];
} else {
    $id = -1;
    $description = '';
    $frequency_struct =  [ "month" => 0,
                           "week" => 0,
                           "day" => 1,
                           "hour" => 0,
                           "minute" => 0,
                           "second" => 0];
    $next = '';
}

?>


<form method="post" id="edit_task">

    <div <?php if ($task_to_edit === null) { echo 'class="hidden"'; } ?>>
        <label for="edit_id">Tâche n°</label>
        <input type="number" id="edit_id" name="edit_id" value="<?php echo $id; ?>" readonly>
    </div>
    <div>
        <label for="edit_description">Description</label>
        <input type="text" id="edit_description" name="edit_description" value="<?php echo $description; ?>" required>
    </div>
    <div>
        <label for="edit_frequency">Fréquence</label>
        <ul id="edit_frequency" >
            <li>
                <input type="number" id="edit_frequency_months" name="edit_frequency_months" value="<?php echo $frequency_struct['month']; ?>" min="0" >
                <label for="edit_frequency_months">mois</label>
            </li>
            <li>
                <input type="number" id="edit_frequency_weeks" name="edit_frequency_weeks" value="<?php echo $frequency_struct['week']; ?>" min="0" >
                <label for="edit_frequency_weeks">semaines</label>
            </li>
            <li>
                <input type="number" id="edit_frequency_days" name="edit_frequency_days" value="<?php echo $frequency_struct['day']; ?>" min="0" >
                <label for="edit_frequency_days">jours</label>
            </li>
            <li>
                <input type="number" id="edit_frequency_hours" name="edit_frequency_hours" value="<?php echo $frequency_struct['hour']; ?>" min="0" >
                <label for="edit_frequency_hours">heures</label>
            </li>
            <li>
                <input type="number" id="edit_frequency_minutes" name="edit_frequency_minutes" value="<?php echo $frequency_struct['minute']; ?>" min="0" >
                <label for="edit_frequency_minutes">minutes</label>
            </li>
            <li>
                <input type="number" id="edit_frequency_seconds" name="edit_frequency_seconds" value="<?php echo $frequency_struct['second']; ?>" min="0" >
                <label for="edit_frequency_seconds">secondes</label>
            </li>
        </ul>
    </div>
    <div>
        <label for="edit_next">Prochaine occurence</label>
        <input type="datetime-local" id="edit_next" name="edit_next" placeholder="Défaut: maintenant + fréquence" value="<?php echo $next; ?>">
    </div>
    <div>
    <?php if ($task_to_edit === null) { ?>
        <button type="reset" >Annuler &#10007;</button>
        <button type="submit" name="update_task" value="add">&#8853; Ajouter une tâche</button>
    <?php } else { ?>
        <button type="submit" >Annuler &#10007;</button>
        <button type="submit" name="update_task" value="edit">&#8853; Modifier la tâche</button>
    <?php } ?>
    </div>
</form>
<?php

include 'footer.inc';

?>
