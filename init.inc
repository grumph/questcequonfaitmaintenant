<?php

ini_set('display_errors', 'On');
error_reporting(E_ALL | E_STRICT);

// prevent XSS
$_GET  = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
$_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

include 'time.inc';

include 'sql.inc';

?>
