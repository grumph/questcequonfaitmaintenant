<?php

$db = new SQLite3('bdd.sqlite');

$db->exec('CREATE TABLE IF NOT EXISTS "tasks"
            (  "id" INTEGER PRIMARY KEY,
               "description" TEXT NOT NULL,
               "frequency" INTEGER NOT NULL,
               "next" DATETIME DEFAULT (DATETIME(CURRENT_TIMESTAMP, \'localtime\')));');


function get_task($id=null)
{
    if ($id === null) {
        $condition = 'ORDER BY next LIMIT 1';
    } else {
        $condition = 'WHERE id=' . $id;
    }
    $result = $GLOBALS['db']->query('SELECT * FROM tasks ' . $condition . ';');
    return $result->fetchArray();
}

function generate_all_tasks()
{
    $request = $GLOBALS['db']->query('SELECT * FROM tasks ORDER BY next');
    while ($row = $request->fetchArray()) {
        yield $row;
    }
}

function postpone_task($id, $postpone_seconds=null)
{
    if ($postpone_seconds === null) {
        $postpone_seconds = get_task($id)['frequency'];
    }
    update_task($id, null, null, "DATETIME(CURRENT_TIMESTAMP, '+" . $postpone_seconds . " seconds', 'localtime')");
}

function update_task($id, $description=null, $frequency=null, $next=null)
{
    if($description === null and $frequency === null and $next === null) {
        return false;
    }
    $update_request = 'UPDATE tasks SET ';
    if ($description !== null) {
        $update_request .= 'description="' . $description . '" ';
        if ($frequency !== null or $next !== null) {
            $update_request .= ', ';
        }
    }
    if ($frequency !== null) {
        $update_request .= 'frequency="' . $frequency . '" ';
        if ($next !== null) {
            $update_request .= ', ';
        }
    }
    if ($next !== null) {
        if (DateTime::createFromFormat('Y-m-d G:i:s', $next) !== FALSE) {
            $update_request .= 'next="' . $next . '" ';
        } else {
            $update_request .= 'next=' . $next . ' ';
        }
    }
    $update_request .= "WHERE id=" . $id . ";";
    $GLOBALS['db']->exec($update_request);
    return true;
}

function delete_task($id)
{
    $GLOBALS['db']->exec('DELETE FROM tasks WHERE id=' . $id . ';');
}

function add_task($description, $frequency, $next='')
{
    $insert_request = 'INSERT INTO tasks (description, frequency, next) VALUES (';
    $insert_request .= "'" . $description . "', ";
    $insert_request .= (string)($frequency) . ', ';
    if ($next != '') {
        $insert_request .= "DATETIME('" . $next . "') " ;
    } else {
        $insert_request .= "DATETIME('" . $GLOBALS['current_date'] . "', '+" . $frequency . " seconds') ";
    }
    $insert_request .= ');';
    $GLOBALS['db']->exec($insert_request);
}

?>
