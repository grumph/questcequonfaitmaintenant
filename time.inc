<?php

date_default_timezone_set('Europe/Paris');

$current_date =  date('Y-m-d H:i:s');

function mydatetime_in_seconds($month, $week, $day, $hour, $minute, $second) {
    return (int)($second
                 + $minute * 60
                 + $hour   * 60 * 60
                 + $day    * 60 * 60 * 24
                 + $week   * 60 * 60 * 24 * 7
                 + $month  * 60 * 60 * 24 * 30.5);
}

function seconds_in_mydatetime($seconds)
{
    $month = (int)($seconds / (60 * 60 * 24 * 30.5));
    $seconds = $seconds % (60 * 60 * 24 * 30.5);
    $week = (int)($seconds / (60 * 60 * 24 * 7));
    $seconds = $seconds % (60 * 60 * 24 * 7);
    $day = (int)($seconds / (60 * 60 * 24));
    $seconds = $seconds % (60 * 60 * 24);
    $hour = (int)($seconds / (60 * 60));
    $seconds = $seconds % (60 * 60);
    $minute = (int)($seconds / (60));
    $seconds = $seconds % (60);
    return [ "month" => $month,
             "week" => $week,
             "day" => $day,
             "hour" => $hour,
             "minute" => $minute,
             "second" => $seconds];
}

function seconds_to_string($seconds)
{
    $res = '';
    $mydatetime = seconds_in_mydatetime($seconds);
    if ($mydatetime['month'] > 0) {$res .= $mydatetime['month'] . " mois ";}
    if ($mydatetime['week'] > 0) {$res .= $mydatetime['week'] . " semaine(s) ";}
    if ($mydatetime['day'] > 0) {$res .= $mydatetime['day'] . " j ";}
    if ($mydatetime['hour'] > 0) {$res .= $mydatetime['hour'] . " h ";}
    if ($mydatetime['minute'] > 0) {$res .= $mydatetime['minute'] . " m ";}
    if ($mydatetime['second'] > 0) {$res .= $mydatetime['second'] . " s ";}
    return $res;
}

?>
